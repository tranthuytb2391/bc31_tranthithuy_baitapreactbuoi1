import logo from './logo.svg';
import './App.css';
import Header from './Header/Header';
import BodyBanner from './BodyBanner/BodyBanner';
import BodyItem from './BodyItem/BodyItem';
import Footer from './Footer.js/Footer';

function App() {
  return (
    <div className="App">
      <Header />
      <BodyBanner />
      <BodyItem />
      <Footer />
    </div>
  );
}

export default App;
