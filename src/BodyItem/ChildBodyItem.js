import React, { Component } from 'react'

export default class ChildBodyItem extends Component {
    render() {
        return (
            <div className='container col-4 py-5'>

                <div div className="card bg-light border-0 h-100" >
                    <div className="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
                        <div className="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4"><i className={this.props.icon} />
                        </div>
                        <h2 className="fs-4 fw-bold">{this.props.ten}</h2>
                        <p className="mb-0">{this.props.mota}</p>
                    </div>
                </div >

            </div >
        )
    }
}
